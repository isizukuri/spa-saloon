SPA salon
======

.. attention::

    To work needed python 3.4 or higher!


Quick start guide
-----------------

Clone.
++++++

.. code-block::

    $ git  clone https://isizukuri@bitbucket.org/isizukuri/prosecutors.space.git
    $ cd prosecutors-space

Install virtualenv.
++++++++++++++++++++

.. code-block::

    $ pyvenv venv
    $ source venv/bin/activate
    (venv)$

Install packages.
+++++++++++++++++

.. code-block::

    (venv)$ pip3 install -r requirements.txt



Synchronize.
++++++++++++

.. code-block::

    (venv)$ pwd
    /some/path/to/projects/prosecutors-space
    (venv)$ cd src/
    (venv)$ ./manage.py migrate
    (venv)$ ./manage.py createsuperuser

Run.
++++

.. code-block::

    (venv)$ pwd
    /some/path/to/projects/prosecutors-space
    (venv)$ cd src/
    (venv)$ ./manage.py runserver 127.0.0.1:7171


