#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model, authenticate
from django.db.models import Q


class RegistrationForm(forms.Form):

    """Form to register users."""
    first_name = forms.CharField(
        label=_('First name'),
    )
    last_name = forms.CharField(
        label=_('Last name'),
    )
    username = forms.CharField(
        label=_('Username'),
    )
    email = forms.EmailField(
        label=_('E-mail'),
    )
    password = forms.CharField(
        label=_('Password'),
        min_length=3,
        max_length=30,
    )
    password_confirm = forms.CharField(
        min_length=3,
        required=False,
        label=_('Password confirm'),
    )

    def __init__(self, *args, **kwargs):
        """Init profile account form."""
        self.request = kwargs.pop('request', None)
        super(RegistrationForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        """The ``username`` field validator."""
        username = self.cleaned_data['username']

        Client = get_user_model()
        query = Q(username__iexact=username)
        existing = Client.objects.filter(query)

        if existing.exists():
            raise forms.ValidationError(
                _('User with such name already exists!')
            )

        return username

    def save(self):
        """Create new user and his profile."""
        # Client model.
        Client = get_user_model()
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        password = self.cleaned_data.get('password')

        # Create new user.
        client = Client.objects.create_user(username=username, email=email,
                                            password=password)
        client.first_name = first_name
        client.last_name = last_name
        client.save()


class LoginForm(forms.Form):
    """The form of the user's authorization."""
    username = forms.CharField(
        label=_('Username'),
    )
    password = forms.CharField(
        min_length=3,
        label=_('Password'),
    )

    def clean(self):
        """The user must be registered."""
        data = super(LoginForm, self).clean()
        username = data.get('username')
        password = data.get('password')

        # Check user rights.
        user = authenticate(username=username, password=password)

        if not user:
            raise forms.ValidationError(_('Access denied.'))

        return data
