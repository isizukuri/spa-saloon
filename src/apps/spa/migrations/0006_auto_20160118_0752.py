# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('spa', '0005_auto_20160117_1830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='applicant',
            field=models.ForeignKey(verbose_name='замовник', to=settings.AUTH_USER_MODEL, to_field='username'),
        ),
    ]
