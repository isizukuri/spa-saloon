# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spa', '0002_auto_20160117_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='cost',
            field=models.DecimalField(decimal_places=2, max_digits=7, verbose_name='вартість', null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='result',
            field=models.BooleanField(verbose_name='одобрено'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.BooleanField(verbose_name='розглянуто'),
        ),
    ]
