# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('spa', '0006_auto_20160118_0752'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': 'application', 'ordering': ['service_date'], 'verbose_name_plural': 'applications', 'get_latest_by': 'created_on'},
        ),
        migrations.AlterField(
            model_name='order',
            name='applicant',
            field=models.ForeignKey(verbose_name='applicant', to=settings.AUTH_USER_MODEL, to_field='username'),
        ),
        migrations.AlterField(
            model_name='order',
            name='comment',
            field=models.TextField(verbose_name='comment'),
        ),
        migrations.AlterField(
            model_name='order',
            name='cost',
            field=models.DecimalField(null=True, verbose_name='cost', blank=True, decimal_places=2, max_digits=7),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_on',
            field=models.DateTimeField(verbose_name='application date and time', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='result',
            field=models.NullBooleanField(verbose_name='approved'),
        ),
        migrations.AlterField(
            model_name='order',
            name='service',
            field=models.CharField(verbose_name='service', choices=[('hair', 'Haircut'), ('nails', 'Nails'), ('cosmetics', 'Cosmetics'), ('tatoo', 'Tatoo'), ('piersing', 'Piersing'), ('massage', 'Massage'), ('other', 'Other')], max_length=10),
        ),
        migrations.AlterField(
            model_name='order',
            name='service_date',
            field=models.DateTimeField(null=True, verbose_name='service date and time', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='solved_on',
            field=models.DateTimeField(verbose_name='solved on', auto_now=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.NullBooleanField(verbose_name='reviewed'),
        ),
    ]
