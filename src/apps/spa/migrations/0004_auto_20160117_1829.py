# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spa', '0003_auto_20160117_1828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='service_date',
            field=models.DateTimeField(verbose_name='дата та час надання послуги', null=True),
        ),
    ]
