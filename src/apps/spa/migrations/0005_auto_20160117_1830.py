# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spa', '0004_auto_20160117_1829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='result',
            field=models.NullBooleanField(verbose_name='одобрено'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.NullBooleanField(verbose_name='розглянуто'),
        ),
    ]
