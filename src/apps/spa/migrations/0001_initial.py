# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Appliсation',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('service', models.CharField(max_length=10, choices=[('hair', 'Стрижка'), ('nails', 'Манікюр або педикюр'), ('cosmetics', 'Косметика'), ('tatoo', 'Тату'), ('piersing', 'Пірсинг'), ('massage', 'Масаж'), ('other', 'Інше')], verbose_name='вид послуги')),
                ('comment', models.TextField(verbose_name='опис заявки')),
                ('cost', models.DecimalField(max_digits=7, decimal_places=2, verbose_name='вартість')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='дата та час замовлення')),
                ('solved_on', models.DateTimeField(auto_now=True, verbose_name='дата та час розгляду')),
                ('service_date', models.DateTimeField(verbose_name='дата та час надання послуги')),
                ('status', models.BooleanField(verbose_name='стан розгляду')),
                ('result', models.BooleanField(verbose_name='результат розгляду')),
                ('applicant', models.ForeignKey(verbose_name='замовник', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
