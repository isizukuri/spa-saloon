# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('spa', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('service', models.CharField(choices=[('hair', 'Стрижка'), ('nails', 'Манікюр або педикюр'), ('cosmetics', 'Косметика'), ('tatoo', 'Тату'), ('piersing', 'Пірсинг'), ('massage', 'Масаж'), ('other', 'Інше')], max_length=10, verbose_name='вид послуги')),
                ('comment', models.TextField(verbose_name='опис заявки')),
                ('cost', models.DecimalField(max_digits=7, verbose_name='вартість', decimal_places=2)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='дата та час замовлення')),
                ('solved_on', models.DateTimeField(verbose_name='дата та час розгляду', auto_now=True)),
                ('service_date', models.DateTimeField(verbose_name='дата та час надання послуги')),
                ('status', models.BooleanField(verbose_name='стан розгляду')),
                ('result', models.BooleanField(verbose_name='результат розгляду')),
                ('applicant', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='замовник')),
            ],
            options={
                'ordering': ['service_date'],
                'verbose_name': 'заявка',
                'get_latest_by': 'created_on',
                'verbose_name_plural': 'заявки',
            },
        ),
        migrations.RemoveField(
            model_name='appliсation',
            name='applicant',
        ),
        migrations.DeleteModel(
            name='Appliсation',
        ),
    ]
