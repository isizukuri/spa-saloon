from django.contrib import admin
from spa.models import Order


class OrderAdmin(admin.ModelAdmin):
    fields = (
        ('applicant', 'service'),
        'comment',
        'created_on',
        'service_date',
        'status',
        'result',
        )
    readonly_fields = (
        'applicant', 'service', 'comment',
        'created_on', 'solved_on',
        )

admin.site.register(Order, OrderAdmin)
