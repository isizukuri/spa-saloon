from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.views.generic.list import ListView

from spa.models import Order
from accounts.mixins import LoginRequiredMixin


class HomePageView(TemplateView):
    """Show homepage."""
    template_name = "this/home.jinja"


class ContactsView(TemplateView):
    """Show contacts page."""
    template_name = "this/contacts.jinja"


class OrderCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """Form to get users orders"""
    model = Order
    fields = ['applicant', 'service', 'comment']
    template_name = 'this/create_order.jinja'
    success_url = '/orders'
    success_message = _('Order succesfully sent, you can now view it status.')
    login_url = '/login'


class OrderListView(ListView):
    """View to display orders for users"""
    template_name = '/this/list_orders.jinja'

    def get_queryset(self):
        return Order.objects.filter(applicant=self.request.user.username)
