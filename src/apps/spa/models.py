#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Order(models.Model):
    """Model for users orders to spa service"""
    applicant = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('applicant'),
        to_field='username',
    )

# Defining tuple of services to use in service model
    services = (('hair', _('Haircut')),
                ('nails', _('Nails')),
                ('cosmetics', _('Cosmetics')),
                ('tatoo', _('Tatoo')),
                ('piersing', _('Piersing')),
                ('massage', _('Massage')),
                ('other', _('Other')),
                )

    service = models.CharField(
        choices=services,
        max_length=10,
        verbose_name=_('service'),
        blank=False,
        )

    comment = models.TextField(
        max_length=256,
        verbose_name=_('comment'),
        )

    cost = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        verbose_name=_('cost'),
        null=True,
        blank=True,
        )

    created_on = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('application date and time'),
        )

    solved_on = models.DateTimeField(
        auto_now=True,
        verbose_name=_('solved on'),
        )

    service_date = models.DateTimeField(
        verbose_name=_('service date and time'),
        null=True,
        blank=True,
        )

    status = models.NullBooleanField(
        verbose_name=_('reviewed'),
        null=True,
        blank=True,
        )

    result = models.NullBooleanField(
        verbose_name=_('approved'),
        null=True,
        blank=True,
        )

    class Meta:
        ordering = ["service_date"]
        verbose_name = _('application')
        verbose_name_plural = _('applications')
        get_latest_by = 'created_on'

    def __str__(self):
        return u"%s %s %s" % (self.applicant,
                              self.service,
                              self.created_on.strftime("%d.%m.%y %H:%M")
                              )
