"""basic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from spa import views as spa_views
from accounts import views as accounts_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', spa_views.HomePageView.as_view(), name='home'),
    url(r'^contacts/$', spa_views.ContactsView.as_view(), name='contacts'),
    url(r'^orders/$', spa_views.OrderListView.as_view(), name='list_order'),
    url(r'^create_order/$', spa_views.OrderCreateView.as_view(),
        name='create_order'),
    # registration and authorization views
    url(r'^register/$', accounts_views.RegistrationView.as_view(),
        name='registration'),
    url(r'^login/$', accounts_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'home'},
        name='logout'),
]
